<?php

    require_once __DIR__ . '/vendor/autoload.php';

    $servername = "localhost";
    $database = "google";
    $username = "root";
    $password = "root";

    $mysqli = new mysqli($servername, $username, $password, $database);
    if ($mysqli->connect_errno) {
        echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }

    $res = $mysqli->query("SELECT * FROM data WHERE age > 18");

    $values = array(["Номер", "Имя", "Фамилия", "Возраст"]);

    $i = 1;
    while ($row = $res->fetch_assoc()) {

        $values[] = [$i, $row['name'], $row['lastname'], $row['age']];
        $i++;
    }

    $googleAccountKeyFilePath = __DIR__ . '/test2-1594638905267-7e0e27f1b187.json';
    putenv( 'GOOGLE_APPLICATION_CREDENTIALS=' . $googleAccountKeyFilePath );

    $client = new Google_Client();

    $client->useApplicationDefaultCredentials();

    $client->addScope( 'https://www.googleapis.com/auth/spreadsheets' );

    $service = new Google_Service_Sheets( $client );

    $spreadsheetId = '1VYiAqdOSYxzOXTMcgr_GXhojgWd1yjA0NZe2xhvoHCY';

    $body    = new Google_Service_Sheets_ValueRange( [ 'values' => $values ] );

    $options = array( 'valueInputOption' => 'RAW' );

    if($service->spreadsheets_values->update( $spreadsheetId, 'Лист1!A1', $body, $options )){
        echo('Данные успешно выгружены.');
    }



?>

